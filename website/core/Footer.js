/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

class Footer extends React.Component {
  docUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    const docsUrl = this.props.config.docsUrl;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    return `${baseUrl}${docsPart}${langPart}${doc}`;
  }

  pageUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    return baseUrl + (language ? `${language}/` : '') + doc;
  }

  render() {
    return (
      <footer className="nav-footer" id="footer">
        <section className="sitemap">
          <a href={this.props.config.baseUrl} className="nav-home">
            {this.props.config.footerIcon && (
              <img
                src={this.props.config.baseUrl + this.props.config.footerIcon}
                alt={this.props.config.title}
                width="66"
                height="58"
              />
            )}
          </a>
          <div>
          <a
              href="http://kelpstrewn.com"
              target="_blank"
              rel="noreferrer noopener">
              Home
          </a>
          </div>
          <div>
          <a
              href="http://akheritage.site"
              target="_blank"
              rel="noreferrer noopener">
              Old buildings in Auckland
          </a>
          </div>
          <div>
          <a
              href="http://sailptchev.org,nz"
              target="_blank"
              rel="noreferrer noopener">
              Sailptchev
          </a>
          </div>
        </section>

        
      </footer>
    );
  }
}

module.exports = Footer;
