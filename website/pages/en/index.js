/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

function Help(props) {
  const {config: siteConfig, language = ''} = props;
  const {baseUrl, docsUrl} = siteConfig;
  const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
  const langPart = `${language ? `${language}/` : ''}`;
  const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;

  const supportLinks = [
    {
      content: `Information on how to map a New Zealand <a href="docs/Introduction.html">farm.</a>
      During lockdown I am offering to produce maps for productive local properties at no charge, free. If you are interested email <a href="mailto:kelp.strewn@gmail.com">kelp.strewn@gmail.com</a>.<br> `,
      image: `${siteConfig.baseUrl}img/im1.svg`,
      imageAlign: 'top',
      imageAlt: 'stages',
      title: 'Farm mapping',
    },
    {
      content: 'I have spent most of my working life in architecture, where I gained the skills needed to make these maps. For the last 10 years I have worked as a conservation architect, focusing on heritage buildings. There is a collection of old maps of Cornwallis, overlaid <a href="docs/Art.html">here.</a> ',
      image: `${siteConfig.baseUrl}img/im2.svg`,
      imageAlign: 'top',
      imageAlt: 'stages',
      title: 'Old Maps',
    },
    {
      content: `Here is some older information, mostly not relevant to this site now. The information on <a href="docs/building_hiveware.html">beekeeping</a> is the most useful.
      `,
      image: `${siteConfig.baseUrl}img/im3.svg`,
      imageAlign: 'top',
      imageAlt: 'stages',
      title: 'Miscellaneous',
    },
  ];


  return (
    <div className="docMainWrapper wrapper">
      <Container className="mainContainer documentContainer postContainer">
        <div className="post">
          <header className="postHeader">
            <h1>Kelp strewn farming</h1>
          </header>
          <p>Supporting productive, precision management of New Zealand farms.</p>
          <GridBlock contents={supportLinks} layout="threeColumn" />
        </div>
      </Container>
    </div>
  );
}

module.exports = Help;
