---
id: Introduction
title: How to map your land, do it yourself (diy), for farmers and smallholders.
---

<div class="resp-container">
    <iframe class="resp-iframe" src="/3dhop/ambury.html" scrolling="no"></iframe>
</div>

3d model Ambury Farm Park, allows measurements and section cutting.

We have been investigating mapping processes for farms and 10 acre block holders. This came out of a need to map a 100 acre bush block that is in extended family ownership, in the Waitakere ranges, west of Auckland.  I then moved to a flatter more farm like site, where easy public access is available. I made both a 3d map, useful for presentation purposes, and available as a downloadable model, and more traditional 2d maps, spatially linked to data and useful for ongoing management.

## Results ##

Here are some examples of output.

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/14569331d33841738761c0b3a05d282a/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

Sketchfab presentation model 100 acre bush block.

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/554925f67fcf40fe985d8eca47d311b7/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

</p>
</div>

Sketchfab presentation model Ambury Farm Park.



<figure>
  <p><img src="/img/am01.jpg" >
  <figcaption>More traditional 2d map image, whole site.
</figcaption>
</figure>

<figure>
  <p><img src="/img/am02.jpg" >
  <figcaption>2d map image, detail.
</figcaption>
</figure>



<figure>
  <p><img src="/img/am02.jpg" >
  <figcaption>2d map image, detail.
</figcaption>
</figure>

The maps are available as high resolution pdfs, able to be measured and with gps coordinates, with acrobat reader dc.

* [Locality](/pdf/ambury_locality.pdf)
* [Detail Area 1](/pdf/ambury_sheet2.pdf)
* [Detail Area 2](/pdf/ambury_sheet3.pdf)
* [Detail Area 3](/pdf/ambury_sheet4.pdf)

## Farm mapping, commercial providers ##

I have had a look at the history of farm mapping and it seems that the process has been around for a while. In New Zealand there have been a number of proprietary solutions and commercial providers. Some of them have come and gone. Others remain, that require a monthly subscription be paid at a relatively high rate. Examples are  Smart Maps by Ravensdown, AgHub by Ballance, FarmIQ and Agrimap.

It seems that these companies use a variety of techniques. These include

* Drone aerial photography
* Conventional aerial photography
* Point location on the ground, using gps recievers. 
* Enhancement of information using cad (computer aided draughting, eg autocad)
* Land Information New Zealand (LINZ) open source data.
* Some providers link their maps into a hub where staff and stock management is also included in a turn key database solution.

Issues with these providers include cost and ownership of information. When a service ceases to trade its platform will not be maintained, and all that recording may be wasted. Flexibility of recording is also an issue as farming operations vary greatly according to land type, proximity to markets, and many other market based and geography based factors. 

In conclusion, a system where farmers own and control their data, and have a guarantee of future platform maintenance through the use of open source software may be able to address many of these issues. The DIY aspect is less important, although this being NZ and the audience being farmers, many will like this.

## Beginning the process ##

I had previously been doing much smaller terrain models of architectural projects that I have been involved with, mostly in suburban settings. 

When I came to model a hundred acre site I found that the method I had been using, which involved contour data derived from a geographic information system (gis) and the use of a 3D modelling program called SketchUp, was not practical. 
  
I scouted around for a solution and discovered a better process based around the use of dedicated gis software. This works much better for larger sites, in terms of data processing and computing resource. The process makes use of free data and open source software. It can be done with no additional cost, above a requirement for a moderately powered laptop, of the type that many people own.  
The process I used is described over a number of pages, accessible in the links in the sidebar / dropdown menu.

## What use is a map? ##

I was a little unsure of what to focus on. I concluded that that depends really on what you plan to do with the model. A layered farm map in 2d in qgis could be a useful day to day work tool, and things such as paddock areas, last date fertilised, water troughs, gates etc could easily go there.

The 3d model is more of a presentation thing in my view. The second model at Ambury Farm does allow measurement. The pdf files allow picking of coordinates, in WGS 84 format, all set for gps use. The use of 3d true length measurements based on topography may well be important in hilly country, as fence lengths and paddock areas will not be accurate if taken from a 2d map.