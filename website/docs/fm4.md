---
id: Loading data
title: Loading data in Qgis
---
 Having obtained the data it needs to be processed to be put into a form usable for farm mapping. The first step is to arrange the data into a directory structure that will allow it be used for a farm map. The structure I suggest is to arrange the main data types in seperate directories under a main project directory. This is done in windows explorer. 
 
   <figure>
  <p><img src="/img/fm07.jpg" >
  <figcaption> Find your downloaded data in the downloads directory.
</figcaption>
</figure>
 
 Move the elevation data to A, the jpg to B and unzip. Do similar with the property boundary shape file.
 
<figure>
  <p><img src="/img/fm08.jpg" >
  <figcaption> Put data in project directory.
</figcaption>
</figure>
 
## Qgis

The  creation of simple traditional farm map with the addition of some 3D will be my focus here. The locality I have chosen is broader than that found in the proceeding farm map example here, as I am planning to repurpose this using an historical chart as well.

So open qgis and save an empty project to the project directory, mine is ambury_locality.

Before you begin it is important to establish correct datum for your map so that it all the imported data ends up in the right place. LINZ data will be downloaded in New Zealand Transverse Mercator projection, if you leave the default set on the LINZ website. Set your project to the same projection. This can be done in the box at the bottom of the screen on the right-hand side.

<figure>
  <p><img src="/img/fm09.jpg" >
  <figcaption> Set cordinate system.
</figcaption>
</figure>

The next step is to put the downloaded Linz data into layers. Go to the layer menu and add the shapefiles vector data as shown in the screen grab. 

<figure>
  <p><img src="/img/fm10.jpg" >
  <figcaption> Add vector layer.
</figcaption>
</figure>

It is a similar process to add the aerial photograph data and the underlying topographic data.  

<figure>
  <p><img src="/img/fm09.jpg" >
  <figcaption> Add raster layer.
</figcaption>
</figure>

Save the project, file menu. This doesn't alter the data in directories 01 to 03, but keeps their arrangement and formatting between sessions.

