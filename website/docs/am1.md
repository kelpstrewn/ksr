---
id: Art
title: A model about landscape and history.
---
## Intro and model ##

Here is a trial app made using blender with two plugins, gis and verge 3d. The technique is quite complex and I won't attempt to explain it here. This is an attempt to make a  model that provides map based views of a landscape. It is a work in progress and a starter for discussion. 
Here is the app. Best viewed on pc with hd screen. Zoom with mmb, pan with lmb. Click black spots for annotations. Use buttons to move through content. 

Under the model is a series of images that relate.
<div class="resp-container">
    <iframe class="resp-iframe" src="http://kelpstrewn.com/svg/aCornwallis/corn.html" scrolling="no"></iframe>
</div>


## Pou ##
![alt text](/img/corn01.jpg "Pou")
![alt text](/img/corn02.jpg "Pou")

## Regional Park ##
![alt text](/img/corn03.jpg "Park")
![alt text](/img/corn03.jpg "Park")
![alt text](/img/corn04.jpg "Park")
![alt text](/img/corn05.jpg "Park")
![alt text](/img/corn06.jpg "Park")
![alt text](/img/corn07.jpg "Park")

## Interp, bach traces ##
![alt text](/img/corn08.jpg "Interp")
![alt text](/img/corn09.jpg "Interp")

## Launching ##
![alt text](/img/corn10.jpg "Launch")

## Shop site? ##
![alt text](/img/corn11.jpg "Shop")

## Orpheus ##
![alt text](/img/corn12.jpg "Ship")
![alt text](/img/corn13.jpg "Ship")

## Papakāinga ##
![alt text](/img/corn14.jpg "Pa")

## Manukau Heads ##
![alt text](/img/corn15.jpg "Heads")