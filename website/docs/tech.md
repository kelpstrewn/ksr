---
id: tech
title: Tech credits
---
## Kelps journey into web tech. ##
I began with a WordPress site and domain and hosting at domainz.co.nz. This was a couple of years ago. It was expensive and my WordPress got hacked. I let the site go and revisited it as a winter project this year. 

I began by following this [advice](https://medium.com/@matryer/cloud-hack-how-to-get-production-ready-free-static-website-hosting-cbc2d25c5771) from medium.com to set up on Google app engine and cloudflare. Worked beautifully. I then needed some content and a method of structuring it. I decided to use a site with content written in markdown and a static site generator. Initially I went for [mdwiki](http://dynalon.github.io/mdwiki) 

This is incredibly easy to use and I am continuing with it on a private site hosted on netlify. The downside with mdwiki is because it makes its HTML on the client computer there is nothing for a search engine to search. This is actually an upside for a private site. Because of the lack of search I then went to [mkdocs](https://www.mkdocs.org/) as a more full blown static site generator. This was after experimenting with Hugo which I found unsatisfactory for a docs style website.

Mk docs is excellent, but I was decided to try others, because of the lack of blog support. I experimented a bit with gatsby.js, but this was two complex for my needs and level of webdev skill. I settled on [docusaurus](https://docusaurus.io/) from the facebook developer team, which is also react based, and this is what is in use now.

Part of what I enjoyed was web animation, and the svg on the front page was made with [Aphilina Animator](https://aphalina.com/). 
