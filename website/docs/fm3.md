---
id: Obtaining data
title: Obtaining free and open source data for NZ
---
 A first step is obtaining data from the land information New Zealand Data Service. This has become a very straightforward process. There is a great deal of data here, much of which is only recently available, and free and in the public domain. Quite often high-definition Lidar data will be available for your rural site, at 1 metre resolution.  If it isn't then the next best option is the use of the 8 metre resolution data to create the underlying topography. 
 
 I have found it is useful to download several data sets for farm map use. One will be a shapefile that gives the property boundaries, one will be the rural aerial map at the best possible resolution and one will be the underlying digital elevation data as discussed above.

 The process is to simply select an area and then go to the top corner and click download.  The following screen grabs go into more detail. 
 <figure>
  <p><img src="/img/fm04.jpg" >
  <figcaption>Screen grab, carry out actions in red, select data.
</figcaption>
</figure>
 
* A. Go to this address in your browser.
* B. Filter on topographic, for 8m DEM, or elevation, for 1m DEM.
* C. Hit the plus sign and make it go red like this.
* D. Navigate from map of NZ to the locality you want, either pan and zoom with mouse or type address.
* E. Activate area selection using this box.
* F. Click diagonal opposite corners to select the area you want. Then click the download box in the top right corner, near E.

There is a limit on download size, and you have to provide an email address, but little else.
 
  <figure>
  <p><img src="/img/fm05.jpg" >
  <figcaption>Here is the window you will see when all is a success.
</figcaption>
</figure>
 
   <figure>
  <p><img src="/img/fm06.jpg" >
  <figcaption>A similar process can be used to get aerial photos, (and shape files for the boundary). The rural photos are available everywhere, but you may find more recent or higher resolution photos in your local council website.
</figcaption>
</figure>
 
