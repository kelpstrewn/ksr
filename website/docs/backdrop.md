---
id: backdrop
title: Walking with Lily
---
## Intro ##
Kelp has a limited range across the Auckland Isthmus, from east to west. The inner Gulf is used for sailing whilst the wild west coast beaches, Kare Kare and Piha, are the common west coast destinations. In the city Kelp is often out and about with his adult disabled daughter and likes to take photographs to make these outings things more interesting. The intention here is that this section will be an amalgam of photo essay, heritage and history musings, and landscape appreciation.
## An outing to Eden Gardens ##

<figure>
  <p><img src="/img/autumn04.jpg" >
  <figcaption>Forgot what this is called, Calla?
</figcaption>
</figure>

<figure>
  <p><img src="/img/autumn05.jpg" >
  <figcaption>Reclining nude.
</figcaption>
</figure>

<figure>
  <p><img src="/img/autumn06.jpg" >
  <figcaption>A flower.
</figcaption>
</figure>

<figure>
  <p><img src="/img/autumn07.jpg" >
  <figcaption>closer in.
</figcaption>
</figure>

<figure>
  <p><img src="/img/autumn08.jpg" >
  <figcaption>Bromeliad.
</figcaption>
</figure>

<figure>
  <p><img src="/img/autumn09.jpg" >
  <figcaption>Water drops.
</figcaption>
</figure>





![](/img/autumn14.jpg)

Volcano
