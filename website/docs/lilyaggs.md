---
id: lilyaggs
title: A walk around Western Park and Auckland Girls Grammar School
---
In 1877 the Education Act was passed by parliament in Wellington. This act established a system of free education for those between the ages of 5 and 13. With the coming of universal suffrage in 1893, with the buoyant economic conditions experienced after 1900 and with the election and re-election of the very progressive Seddon Government the extension of this free education system to encompass older children became a priority in the new century.

The grammar school tradition is English derived. Some English grammar schools can demonstrate an unbroken 600 year long history. The 'grammar' referenced in the type was originally Latin grammar.

>It seems perfectly necessary that for studies to have good effect, they should be carried on without intermission. Rut the crux of the whole question is money.
	
>There is another aspect to the matter which concerns girls and women more than it does men. Mr Seddon has touched upon it, in answering the objection made by some secondary schools that they have no room for an influx of primary school pupils. "Out" he says, "should go the children of tender years whose parents look down on those who send their children to public schools." If there is one thing more detestable, and at the same time ridiculous, in such a community as that. of New Zealand, it is the rank snobbery exhibited by girls who seem to acquire it, while, and by virtue of, attending a school where fees are paid. I have known girls, who have won scholarships which will take them to the secondary school. They have been apparently nice-natured girls. But after a few months of secondary school they seem to lose their eyesight when quondam teachers and pupils of the primary school meet them in the street. The next step is contempt for their simple parents, their father's occupations become professions, or if this is impossible, are carefully hidden from the knowledge of other snobbish friends. 
	
*March, 1903 ILLUSTRATED MAGAZINE.*

## Grammar schools ##
In Auckland the first grammar school for boys opened in Howe St, in the disused immigration barracks, in 1869. The school soon relocated to disused barracks buildings in Albert Park. In 1888 girls began to attend as well, as Auckland Girls High School was forced to close by the depression. At this time the Grammars catered only to paying students. In the new century free places were introduced and the roll began to expand rapidly. In 1905 it was decided to construct a new purpose built school building for the girls..The building was designed by a joint architectural team. Henry Wade was the architect for the Auckland Education Board. W.A. Cummings and G.S. Goldsbro were in and out of partnership with Wade at various times. The building was opened in 1909

![elevation](/img/lily_aggs01.jpg)

*Front elevation.* Note asymmetry of entrance and picturesque arrangement of the belvedere.

It was well received on opening, and particular comment was made regarding the pleasant site high on the airy ridge overlooking the harbour and adjacent to Western Park. The assembly hall was the most admired space inside the building.

## The architecture ##
Stylistically, the building is interesting. It has been described elsewhere as "Queen Anne". However, Queen Anne revival is generally acknowledge to be a classicising style, and this building is not conventionally classical. The work of Richard Norman Shaw in England is likely to have been an influence. Shaw drew on earlier exemplars than the Queen Anne did. He made use of medieval and Jacobean motifs and arranged these freely and asymmetrically in his work. This work was related to that of the architects of the Arts and Crafts movement, but occurred alongside of, rather than in the movement.

![elevation1](/img/lily_aggs02.jpg)

*The school now.* Some fairly bad building around the original block has happened, at least the street elevation is intact.

This approach allowed for a free arrangement in both plan and elevation. It related well to the medieval traditions implicit in the English Grammar School model. In the Auckland context the use of the style can be regarded as thoroughly progressive.

![barracks](/img/lily_aggs03.jpg)

## The immigrant barracks in Howe St.
This used to be close by the schools and the park. We took this old photo on our walk as a reference and tried to figure out how it relates to the gound now. We began at Smith St, and decided that the likely site for the camera in the old photo is at the end of Smith St. We walked down into the park and cut through the school to Howe St. There is alot more vegetation in the park now, and it was difficult to be certain, but I think the barracks were on the school site.
![barracks](/img/lily_aggs04.jpg)
