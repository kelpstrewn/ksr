---
id: Setting up
title: Set up; hardware and software.
---
I'm suggesting the use of qgis. Qgis is open source software, and is therefore free. It has a massive user database, and a community of users and programmers who work on the code without charge. This community guarantees that this software platform will be maintained and developed into the future.  Annual reports from the project are available [here](https://www.qgis.org/en/site/getinvolved/governance/annual_reports/index.html).

This is a very powerful spatial mapping tool that also allows the creation of 3D models from a plug-in. Qgis supports the linking of text in table based information to polygons or points on the map. The linking is accomplished through the use of a locally held database is or even more simply through the use of Excel files and data can move both ways from the map to the database from the database to the map. This allows for the creation of schedules of paddock areas for example and records. Almost anything can be recorded and linked to the map. Choices will depend on the nature of your operation, but could include fertiliser application, location of water tanks, fence lengths and condition, tree ages and locations for growers, etc. 

## What computer do I need? ##
My system dates from 2014 and is an  HP EliteBook 850 G1, a business grade laptop with 16gb of ram and an i7 processor. These sell on trade me for about now. Windows 10 is recommended.

## Get the software ##
The stable standalone 64 bit release is [here](https://qgis.org/downloads/QGIS-OSGeo4W-3.4.14-1-Setup-x86_64.exe).
This should work for most, download and install. More options are [available](https://www.qgis.org/en/site/forusers/download.html) for apple etc. 

Start it up and get some plugins. 

<figure>
  <p><img src="/img/fm01.jpg" >
  <figcaption>Screen grab, carry out actions in red, select plugin.
</figcaption>
</figure>

<figure>
  <p><img src="/img/fm02.jpg" >
  <figcaption>Find qgisthreejs and check the box.
</figcaption>
</figure>

## Set up a directory structure ##
Qgis keeps its data as a collection of individual files. To keep your project orderly a systematic approach to structuring directories is recommended. 

<figure>
  <p><img src="/img/fm03.jpg" >
  <figcaption>Recommended directory structure, windows explorer.
</figcaption>
</figure>