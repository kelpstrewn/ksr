---
id: books
title: Book Reviews - Digital Is Destroying Everything
---
Here are some of the things Kelp has been reading.


## Digital Is Destroying Everything: ##  
What the Tech Giants Won't Tell You about How Robots, Big Data, and Algorithms Are Radically Remaking Your Future, 2015, Andrew V. Edwards.

This one is not particularly well written, but it does appear at first glance to take a strikingly negative view of all things digital. I am reminded of something in Victor Hugo's Notre-Dame de Paris.

> Our lady readers will pardon us if we pause for a moment to seek what could have been the thought concealed beneath those enigmatic words of the archdeacon: This will kill that. The book will kill the edifice.

> To our mind, this thought had two faces. In the first place, it was a priestly thought. It was the affright of the priest in the presence of a new agent, the printing press. It was the terror and dazzled amazement of the men of the sanctuary, in the presence of the luminous press of Gutenberg. It was the pulpit and the manuscript taking the alarm at the printed word: something similar to the stupor of a sparrow which should behold the angel Legion unfold his six million wings. It was the cry of the prophet who already hears emancipated humanity roaring and swarming; who beholds in the future, intelligence sapping faith, opinion dethroning belief, the world shaking off Rome. It was the prognostication of the philosopher who sees human thought, volatilized by the press, evaporating from the theocratic recipient. It was the terror of the soldier who examines the brazen battering ram, and says: The tower will crumble. It signified that one power was about to succeed another power. It meant, The press will kill the church.

So when you think about it, as Mr Edwards has done, digital is killing heaps of things.

The list includes:-
 	
- The music industry
- Journalism and newspapers
- How we know things
- Most clerical and administrative jobs
- Manufacturing
- Customer service
- Retail
- Etc


## Profound change, rapid change. ##
The author lists all this, and notes who the winners and losers are. He is fairly agnostic about the merits and demerits though. For example with regard to the music industry he notes that big music, the stadia rock bands of the 1960s and 1970's, was bloated, corrupt, and finally, to many, not that interesting.

Now we have small concerts, participation, microforums etc. Better or worse? Food for thought."




