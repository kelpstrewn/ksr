---
id: layers
title: Layers
---

The next step is to arrange and format the data in the imported layers. The layers can be moved so that they display in whatever order you want. This effects  visibility and what is seen on screen. 

As an example try changing the order of the  boundary data. If it is at the top of the screen it will obscure the other layers. As you move it down more data from the other layers will appear. 

The boundaries layer has a fill that we don't want. 

Right click to open the properties box turn off the fill for this layer so you can see what is below. The next layer down should be an aerial photograph and the bottom layer should be the Geo Tiff. 

These three layers will give you the base from which you can do different, more advanced things with qgis.
