---
id: books1
title: Book Review - Madame Blavatsky's Baboon 
---
## Madame Blavatsky's Baboon ##
A History of the Mystics, Mediums, and Misfits Who Brought Spiritualism to America, Peter Washington, Shocken, 1993
This book is about religion. After Darwin there was turmoil within societies that were ordered by Christian belief. Alternatives arose. This book traces these, from the 19th century through to the guru religions of west coast America in the 20th.

The truth here is much stranger than fiction, or what could be imagined. Strange blends of eastern religions arose, often around charismatic leadership. These leaders were highly eccentric, and morally dubious. Madame Blavatsky is an eminent example.

![Page from new age](/img/books01.jpg)The author's tone is sceptical, and at times he strays from giving the facts into generalisation. However, given the lack of scholarship in this area, and the ongoing recycling of eastern traditions in the west, this is very forgiveable. I wanted to know about this stuff, and when very interested can easily forgive lapses in an authors tone.   I first read this book about 20 years ago, when we as a family were considering sending my daughter to a Anthoposophical curative education community, as she has a disability. The book didn't scare me off, Steiner comes out well in comparison to the others. My daughter is still there, and has thrived. My mother in law also read it back then on my recommendation. She read straight through to, in almost one sitting. Revisiting now I am enjoying this book in a different way. The salacious detail around Krishnamurti and Leadbeater, and Gurdjieff with his levitation, is less surprising now. Now I am enjoying the detail, numbers involved, and the successes achieved by people striving to do things differently, often with women in the leadership roles. Rereading now, it is easy to check and follow up intriguing references, in a way that wasn't possible 20 years ago. There are many interesting references, throughout this book. One example is a reference to a magazine called The New Age which began weekly publication about 1907. All editions are available online at The New Age. In fact, this whole archive is a treasure trove for the period, and for a study of early modernism. So thanks very much Mr Washington, this book has stood a test of time.



.