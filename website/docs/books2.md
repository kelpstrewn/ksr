---
id: books2
title: Book Review - Afterburn 
---
## Afterburn: Society beyond fossil fuels. ##
Richard Heinberg, New Society, 2015
Afterburn is about the great conflagration that is concealed within internal combustion engines of various types. It is about the prospects for continuing with this and how a forced transition to other ways of producing and using energy will look.

It is written almost in the style of a business motivation book, and comprises a series of essays, written over the last 5 years or so.
 
The author, Richard Heinberg, is more activist then academic. He works for the post carbon institute, and was been involved in journalism about energy and the environment for 15 years or so. Based on an internet search he is well regarded but not as high profile as other individuals in the same field. The book is published by New Society, an activist imprint.  

The author gives reasons why we must change and a schema for  how that might occur. Backed by ideas from the discipline of cultural materialism he identifies infrastructure, structure and superstructure. Infrastructure is relationship of society to environment, structure is modes of production, superstructure is the ideation that drives all that. 

To change we need to change the superstructure, but that will only proceed if the change is driven through at the level of infrastructure.

I have a strong interest in infrastructure, perhaps early visits to the Waikato power stations were formative, and so agree. Much of what Mr Heinberg says is well and succinctly put, so will let him speak for himself, as follows.
## Afterburn quotes ##
> **THE FIGHT OF THE CENTURY**

>Due to energy limits, overwhelming debt burdens, and accumulating environmental impacts, the world has reached a point where continued economic growth may be unachievable. Instead of increasing its complexity, therefore, society will—for the foreseeable future, and probably in fits and starts—be shedding complexity.

----------
  
 
>A. **Continued (ever-more desperate) pursuit of business-as-usual.** In this scenario, policy makers try restarting economic growth with stimulus spending and bailouts; all efforts are directed toward increasing, or at least maintaining, the complexity and centralization of society. Deficits are disregarded."


----------
 
>B. **Simplification by austerity.**
 
A shrinking economy means declining tax revenues, which make it harder for governments to repay debt. In order to avoid a credit downgrade, governments must cut spending. This shrinks the economy further, eventually resulting in credit downgrades anyway. That in turn raises the cost of borrowing. So government must cut spending even further to remain credit-worthy. The need for social spending explodes as unemployment, homelessness, and malnutrition increase, while the availability of social services declines. The only apparent way out of this death spiral is a revival of rapid economic growth. But if the premise above is correct, that is a mere pipedream.

----------
 
>C. **Centralized provision of the basics.** In this scenario, nations directly provide jobs and basic necessities to the general public while deliberately simplifying, downsizing, or eliminating expendable features of society such as the financial sector and the military, and taxing those who can afford it—wealthy individuals, banks, and larger businesses—at higher rates.
 

----------

>D. **Local provision of the basics.** Suppose that, as economies contract, national governments fail to step up to provide the basics of existence to their citizens. Or (as just discussed) suppose those efforts wane over time due to an inability to maintain national-scale infrastructure. In this final scenario, local governments, ad hoc social movements, and nongovernmental organizations could organize the provision of basic necessities. These groups could include small businesses, churches and cults, street gangs with an expanded mission, and formal or informal cooperative enterprises of all sorts.

>In the absence of global transport networks, electricity grids, and other elements of infrastructure that bind modern nations together, whatever levels of support that can originate locally would provide a mere shadow of the standard of living currently enjoyed by middle-class Americans or Europeans. Just one telling example: we will likely never see families getting together in church basements to manufacture laptop computers or cell phones from scratch. The ongoing local provision of food and the simplest of manufactured goods is a reasonable possibility, given intelligent, cooperative effort; for the most part, however, during the next few decades a truly local economy will be mostly a salvage one.