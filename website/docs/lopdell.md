---
id: lopdell
title: A visit to Te Uru Waitakere Contemporary Gallery / Lopdell House.
---
Saturday July 28th 2018: Today Lily and I visited this Auckland regional art facility. We went first to the green modern bit. 

![lopdell](/img/lopdell1.jpg)
*Sympathy between the spanish mission "pub with no beer" and the new gallery is achieved through scale, and by aligning the top part of the new building with parts of the old building.*

![lopdell](/img/lopdell2.jpg)
*The interior, the architects were Mitchell Stout, and the builder was NZ Strong.*

![lopdell](/img/lopdell4.jpg)
*The views from the ridge are celebrated, Lily floats. The restoration of the roof terrace of the old building was an important part of the project, and visitors should take the lift to that too.*

There were a number of excellent exhibitions on in the gallery, a great variety. The exhibition from Te Kawerau a Maki had exquisite objects carved by John Collins of Te Kawarau. Here is a [link](https://timespanner.blogspot.com/2009/01/art-at-olympic-park.html) to another of John's carvings, and a walk at Olympic Park that Lily and I will do soon.

![lopdell](/img/lopdell3.jpg)
*The models were used to produce the drawings. Memories of an unbuilt city at Cornwallis.*

There was a series of large pen and ink drawings which also dealt with colonial history.

http://teuru.org.nz/index.cfm/whats-on/calendar/stephen-ellis-headforemost/

![lopdell](/img/lopdell5.jpg)
*Minimalist, postmodern, ephemeral history.*

There was a very postmodern literary exhibition with typewriters that stood in strong contrast to the first two. 

http://teuru.org.nz/index.cfm/whats-on/calendar/gabrielle-amodeo-blind-carbon-copy-an-open-love-letter/