---
id: farm map
title: How to map your land, do it yourself, for farmers and smallholders.
---
We have been investigating mapping processes for farms and 10 acre block holders. This came out of a need to map a 100 acre bush block that is in extended family ownership, in the Waitakere ranges, west of Auckland.  I then moved to a flatter more farm like site, where easy public access is available. I achieved both a 3d map, useful for presentation purposes, and available as a downloadable model, and more traditional 2d maps, spatially linked to data and useful for ongoing management.

## Results ##
Sketchfab presentation model 100 acre bush block.

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/e99a426686244e03a3273445fa6549a3/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

Sketchfab presentation model Ambury Farm Park.

3dhop presentation model Ambury Farm Park, allows measurements and point picking.

More traditional 2d map image and in hi res pdf format.

## Farm mapping, commercial providers ##

I have had a look at the history of farm mapping and it seems that the process has been around for a while. In New Zealand there have been a number of proprietary solutions and commercial providers. Some of them have come and gone. Others remain, that require a monthly subscription be paid at a relatively high rate. Examples are  Smart Maps by Ravensdown, AgHub by Ballance, FarmIQ and Agrimap.

It seems that these companies use a variety of techniques. These include

* Drone aerial photography
* Conventional aerial photography
* Point location on the ground, using gps recievers. 
* Enhancement of information using cad (computer aided draughting, eg autocad)
*Land Information New Zealand (LINZ) open source data.
* Some providers link their maps into a hub where staff and stock management is also included in a turn key database solution.

Issues with these providers include cost and ownership of information. When a service ceases to trade its platform will not be maintained, and all that recording may be wasted. Flexibility of recording is also an issue as farming operations vary greatly according to land type, proximity to markets, and many other market based and geography based factors. 

In conclusion, a system where farmers own and control their data, and have a guarantee of future platform maintenance through the use of open source software may be able to address many of these issues. The DIY aspect is less important, although this being NZ and the audience being farmers, many will like this.

## Beginning the process ##

I had previously been doing much smaller terrain models of architectural projects that I have been involved with, mostly in suburban settings. 

When I came to model a hundred acre site I found it the method I had been using, which involved contour data derived from a geographic information system (gis) and the use of a 3D modelling program called SketchUp, was not practical. 
  
I scouted around for a solution and discovered a better process based around the use of dedicated gis software. This works much better for larger sites, in terms of data processing and computing resource. The process makes use of free data and open source software. It can be done with no additional cost, above a requirement for a moderately powered laptop, of the type that many people own.  
The process I used is described over a number of pages, accessible in the links in the sidebar / dropdown menu.

## What use is a map? ##

I was a little unsure of subject boundaries and what to focus on. I concluded that that depends really on what you plan to do with the model. A layered farm map in 2d in qgis could be a useful day to day work tool, and things such as paddock areas, last date fertilised, water troughs, gates etc could easily go there.

The 3d model is more of a presentation thing in my view. The second model at Ambury Farm does allow measurement, and picking of coordinates, in WGS 84 format, all set for gps use. The use of 3d true length measurements based on topography may well be important in hilly country, as fence lengths and paddock areas will not be accurate if taken from a 2d map.

farmmap2
---
id: Setting up
title: Installing software and obtaining data.
---

I'm suggesting the use of qgis. Qgis is open source software, and is therefore free. It has a massive user database, and a community of users and programmers who work on the code without charge. This community guarantees that this software platform will be maintained and developed into the future.  Annual reports from the project are available [here](https://www.qgis.org/en/site/getinvolved/governance/annual_reports/index.html).

This is a very powerful spatial mapping tool that also allows the creation of 3D models from a plug-in. Qgis supports the linking of text in table based information to polygons or points on the map. The linking is accomplished through the use of a locally held database is or even more simply through the use of Excel files and data can move both ways from the map to the database from the database to the map. This allows for the creation of schedules of paddock areas for example and records. Almost anything can be recorded and linked to the map. Choices will depend on the nature of your operation, but could include fertiliser application, location of water tanks, fence lengths and condition, tree ages and locations for growers, etc. 

## What computer do I need? ##
My system dates from 2014 and is an  HP EliteBook 850 G1, a business grade laptop with 16gb of ram and an i7 processor. These sell on trade me for about now. Windows 10 is recommended.

## Get the software ##
The stable standalone 64 bit release is [here](https://qgis.org/downloads/QGIS-OSGeo4W-3.4.14-1-Setup-x86_64.exe).
This should work for most, download and install. More options are [available](https://www.qgis.org/en/site/forusers/download.html) for apple etc. 

Start it up and get some plugins. 

<figure>
  <p><img src="/img/farm01.jpg" >
  <figcaption>Screen grab, carry out actions in red.
</figcaption>
</figure>

## Set up a directory structure ##
Qgis keeps its data as a collection of individual files. To keep your project orderly a systematic approach to structuring directories is recommended. 

<figure>
  <p><img src="/img/farm02.jpg" >
  <figcaption>Recommended directory str.
</figcaption>
</figure>

 obtaining data from the land information New Zealand Data Service this is a first step and has become very straightforward there is a great deal of data here much of which is only recently available and arrived and quite often a high-definition lyda Lida data will be available for your rural site if it isn't then it seems that the next next option is the use of the ATM resolution data to create the underlying topography I have found it is useful to download free data sets for better use one will be a shapefile that gives the property boundaries 1 will be the rural aerial map at the best possible resolution at 1 will be the underlying dm3 data as discussed above process is to simply select an area and then going to the top corner and click download the screen grab shows it next page having a tame the data needs to be processed to be put into a form usable for farm mapping first step is to arrange the data into a directory structure that will allow it be used for a farm map the structure I suggest is that swallows the downloaded data which is in the following directories in Linz Data Service should be placed in the relevant directory under the proposed data structure for further processing  but before going into the smaller part of qgis creation of simple traditional farm map with the addition of the data visualisation and 3D will be my focus open qgis and then begin to add you win Sabre project for your map at the top of the proposed is that data structure that is in the file menu once next step is turn port the downloaded Linz data into layers go to the layer menu and Ed the shapefiles vector data as shown in the screen grab below who's a similar process to add the aerial photograph data and the underlying topographic data Chisholm The Geo to file before you begin it is important to establish correct datum for your maps for your map so that it all the imported data ends up in the right place lims data will be downloaded in New Zealand transverse Mercator projection if you leave the default on the links website so set your project to the same projection this can be done in the box at the bottom of the screen on the right-hand side next page arranging and formatting your data the latest can be stepped when over the other in different orders and this affects the visibility and what is seen on screen put the bound the boundary of shapefile data Victor data at the top of the screen on the top of a list and write ticket to open the properties box turn off the fill for this layer so you can see what is blood beloved the next light down should be there or photograph and the bottom layer should be the Geo Tiff these three layers will give you the base from which you can do different more advanced program things with qgess the first of these might be the addition off creation of a 3D map creating a 3D map first step is to load qgis 3 JS plugin which can be found here going to the qgis menu top of the screen find it on the list and install it once installed at was shut in the menu at Little Green Triangle the sun and the screen grab create the 3D map simply select the DM layer and then hit the trying to click the triangle it will bring up some options and and will display a preview of the map is a starting point it should be displaying the aerial photograph predict projected of photography this is a reasonable starting point and a quick visualisation visualisation of your land much more can be done though and I will do to go to page further on tomorrow at Bass techniques next page tracing in polygons