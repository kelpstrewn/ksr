---
id: bees
title: My experiences as a beginning hobby beekeeper.
---
## Starting out.
I began beekeeping about 5 years ago in the spring of 2015. I joined the Auckland Bee Club which meets not far from where I live and attended the meetings to find out how to do it. At the same time I built my hiveware. I used pallet timber which I glued where necessary to obtain large enough pieces. A table saw was crucial to this process. 

I also made all my own frames. I don't recommend this as it is quite time consuming. For people who have the ability to do overtime and just make some extra money it is more efficient just to purchase your hiveware in kit form. 

![hive](/img/bees01hive.jpg)I began with two hives and located these in the back garden. Nucs (nucleus colonies) came from Te Atatu and my first year was extremely successful. There were good bee numbers, low mite counts and a good honey harvest. 

The hives made it through the winter. The next year was not as successful. During the winter I treated with apistan and for follow up treatments I used oxalic acid fuming. This seemed to be quite ineffectual and I think mite numbers reduced the harvest. 
## AFB.
The season was still ok though and all part of the learning process. The following spring was not so good and by Christmas it was clear that the hives were in trouble. The required check for American foulbrood showed that it was present in the hives. Accordingly, I had to burn both hives.![door](/img/bees02hivedoor.jpg)

The business of poisoning the hives with petrol and then setting them on fire was very distressing. The heat and size of the fire was quite problematic in a suburban setting as well.
## A break.
Because of this I decided to give myself a year off beekeeping and to reassess what I was doing. It seems that in the suburbs where there are large concentrations of bee keepers if one beekeeper is not managing foulbrood properly it is likely to become quite prevalent. Westmere was a hotspot at this time.
## Back into it.
We are now keeping bees on a rural property in the bush at Kare Kare. The team is my cousin and I. We are using bench hives and are keeping hive numbers to two or three. we recommenced last spring about November with the capture of a swarm, and split to make another hive, after the harvest was finished.
