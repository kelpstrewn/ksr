---
id: Digitising data
title: Digitising custom data
---
To have a useful map you will want to go beyond the public domain data from linz. 

Here is a method for either making a dead simple preliminary map, or for getting a data layer for import into qgis, with all its capabilities. 

Get a google account if you don't already have one (free) and login. Go to [my maps]( 