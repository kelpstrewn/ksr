---
id: 3d maps
title: Making 3d maps
---
The first of these might be the creation of a 3D map. 

## Creating a 3D map 
The first step is to load the qgis 3 JS plugin. Detail about this can be found [here](https://qgis2threejs.readthedocs.io/en/docs/Tutorial.html).

Going to the qgis menu at the top of the screen, find it on the list and install it. 

Once installed zoom the screen to show in plan the extent of the land you wish to see in the 3d model.  Then click the Little Green Triangle. The model will display. check and uncheck the layers at the side.  As a starting point it should be displaying the aerial photograph  projected onto the terrain of your land. 

This is a reasonable starting point and a quick visualisation of your land. Much more can be done though.  At this stage save the model as a complete web page using either the local viewing option, or for use under a domain, if you are uploading to a website you own.