## Heritage and character have moved.
The drawings and articles are now to be found at [akheritage.site](https://akheritage.site ). 
Kelpstrewn.com now represents the online persona, Kelp Strewn Rock.
<div class="resp-container">
    <iframe class="resp-iframe" src="https://kelpstrewn.com/svg/frontmatter.html" scrolling="no"></iframe>
</div>
Kelp Strewn is a nom-de-plume, a name that a writer uses instead of his or her real name. There is a tradition of great female writers using noms-de-plume.  The Brontë sisters, George Sand, and George Eliot are examples.  
I first used this nom-de-plume decades ago when I was reading a lot of nautical history and associated sea writing. 
As I recall it there were a series of links that led me to this nom-de-plume. It began with "The Totorere Voyage" which I found remaindered in a Devonport bookshop. This is a book in the tradition of Johnny Wray's "South Sea Vagabonds". It is about the work of Gerald Stanley Clark MBE  who built an Allan Wright "Nova" design yacht himself. The design was much modified by Mr Clark, and included extra length and extra scantlings for strength. 
<div class="flex-container">
<div style="display: block; width: 400px; margin: auto; padding:20px;" >
<figure>
  <p><img src="/img/ksr01.jpg" >
  <figcaption>Cover; The Totorere Voyage.</figcaption>
</figure>
</div>
</div>
He spent many years in the Southern Ocean counting seabirds as a retiree. The coastlines of the south of South America are a big part of this story and captured my imagination. 
At the same time I borrowed from my father-in-law a copy of "Byron of the Wager" an historical account of a wreck on the same coast. My father-in-law joined the New Zealand section of the Royal Navy in 1938 and visited these shores during the war as a stoker and petty officer on Royal Navy capital ships. He later served in the New Zealand Navy's minesweepers when that navy was established. 
<div class="flex-container">
<div style="display: block; width: 600px; margin: auto; padding:20px;" >
<figure>
  <p><img src="/img/ksr02.jpg" >
  <figcaption>Wreck in 1741</figcaption>
</figure>
</div>
</div>
These things led me to accounts of the Yahgan, people who lived on the littoral edge rocks at the very bottom of the continent.
<div class="flex-container">
<div style="display: block; width: 600px; margin: auto; padding:20px;" >
<figure>
  <p><img src="/img/ksr03.jpg" >
  <figcaption>On the rocks</figcaption>
</figure>
</div>
</div>
The appeal of sailing as a sport or as a pastime lies partially in the enormous literature that comes with it, and it pleases me to draw from this tradition. 
A further reinforcing factor lies with my early childhood memories on the west coast of Auckland, where we used to fish off the rocks. The "cauldron" at Karekare beach was our spot, and this has a rock in its centre that trails kelp. 
This favourite image of mine, now housed in the boarding house at Whatipu, captures it. 
<div class="flex-container">
<div style="display: block; width: 600px; margin: auto; padding:20px;" >
<figure>
  <p><img src="/img/ksr04.jpg" >
  <figcaption>On the rocks. The artist is Cameron Johnson.</figcaption>
</figure>
</div>
</div>



